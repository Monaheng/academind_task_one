// 1) Create new app, output an AppBar and some text below it
// 2) Add button that changes the text
// 3) Split your work into three widgets: App, TextControl & Text

import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Flutter Tutorial',
    home: TutorialHome(),
  ));
}

class TutorialHome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CounterState();
  }
}

class _CounterState extends State<TutorialHome> {
  int _counter = 0;

  void _increment() {
    setState(() {
      _counter += 1;
    });
  }

  void _decrement() {
    setState(() {
      _counter -= 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    // Scaffold is a layout for the major Material Components.
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          tooltip: 'Navigation menu',
          onPressed: null,
        ),
        title: Text('Example title'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            tooltip: 'Search',
            onPressed: null,
          ),
        ],
      ),
      // body is the majority of the screen.
      body: Center(
          child: Column(
        children: <Widget>[
          RaisedButton(
            onPressed: _increment,
            child: Text('LetsAdd'),
          ),
          Text('You have $_counter days left to live')
        ],
      )),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            tooltip: 'Add', // used by assistive technologies
            child: Icon(Icons.add),
            onPressed: _increment,
          ),
          SizedBox(width: 10),
          FloatingActionButton(
            tooltip: 'Decrement',
            child: Icon(Icons.remove),
            onPressed: _decrement,
          )
        ],
      ),
    );
  }
}

